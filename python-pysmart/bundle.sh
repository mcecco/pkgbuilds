#!/bin/bash

dir="$(dirname "$(readlink -f "$0")")"

. "$dir/../util.sh"

build_package() { # $1 - package name, $2 - python binary
	export python_binary="$2"

	local package_build_dir="$(prepare_build_directory "$build_dir" "$1")"
	cp "$dir/../_common/"* "$package_build_dir/"
	cp "$dir/template/"* "$package_build_dir/"
	envsubst '$python_binary' < "$dir/template/PKGBUILD" > "$package_build_dir/PKGBUILD"
}

build_dir="${1:-}"

if [ -n "$build_dir" ] && ! [ -d "$build_dir/" ]; then abort 2 'The provided build directory does not exist, aborting...'; fi

for binary in 'python' 'python2'; do
	echo "$binary-pysmart"
	if [ -n "$build_dir" ]; then build_package "$binary-pysmart" "$binary"; fi
done
