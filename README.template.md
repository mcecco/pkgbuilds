# PKGBUILDs

This repo contains PKGBUILD files for the following packages:
$package_list

## Local development

Builds packages changed since the last commit and updates package list in README:

```shell
./pkgbuild-repo.sh dev
```

## AUR deployment

Builds packages changed in the last commit and pushes packages that changed to the AUR:

```shell
./pkgbuild-repo.sh prod
```

## Arguments

`-a` - builds all packages regardless of if they changed or not

## Copyright

### Scripts used to manage the repo (`pkgbuild-repo.sh`, `util.sh`, `*/bundle.sh`)

Licensed under [GPLv3](LICENSE.GPLv3).

### weatherspect man pages (`weatherspect{,-git}/weatherspect.1`)

Based on the [weatherspect README](https://github.com/AnotherFoxGuy/weatherspect/blob/master/README.md) licensed under [GPLv2](https://github.com/AnotherFoxGuy/weatherspect/blob/master/gpl.txt) (changes: transformed the Markdown readme into the man page format, date: 2019/12/30), therefore licensed under [GPLv2](LICENSE.GPLv2) as well.

### All other files

Released into the public domain or where not possible, licensed under the [Unlicense](LICENSE.UNLICENSE).
