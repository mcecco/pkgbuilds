# PKGBUILDs

This repo contains PKGBUILD files for the following packages:
* [absolutely-proprietary](https://aur.archlinux.org/packages/absolutely-proprietary/)
* [bitw-git](https://aur.archlinux.org/packages/bitw-git/)
* [cecilia](https://aur.archlinux.org/packages/cecilia/)
* [cecilia-git](https://aur.archlinux.org/packages/cecilia-git/)
* [certbot-dns-vultr](https://aur.archlinux.org/packages/certbot-dns-vultr/)
* [certspotter](https://aur.archlinux.org/packages/certspotter/)
* [certspotter-git](https://aur.archlinux.org/packages/certspotter-git/)
* [cloudprint-cups](https://aur.archlinux.org/packages/cloudprint-cups/)
* [cloudprint-cups-git](https://aur.archlinux.org/packages/cloudprint-cups-git/)
* [dmenufm](https://aur.archlinux.org/packages/dmenufm/)
* [electron3-bin](https://aur.archlinux.org/packages/electron3-bin/)
* [ferdi](https://aur.archlinux.org/packages/ferdi/)
* [ferdi-bin](https://aur.archlinux.org/packages/ferdi-bin/)
* [ferdi-git](https://aur.archlinux.org/packages/ferdi-git/)
* [flow-bin](https://aur.archlinux.org/packages/flow-bin/)
* [gnome-keyring-import-export-hg](https://aur.archlinux.org/packages/gnome-keyring-import-export-hg/)
* [golang-github-atotto-clipboard](https://aur.archlinux.org/packages/golang-github-atotto-clipboard/)
* [golang-github-godbus-dbus](https://aur.archlinux.org/packages/golang-github-godbus-dbus/)
* [golang-github-google-uuid](https://aur.archlinux.org/packages/golang-github-google-uuid/)
* [golang-github-knq-ini](https://aur.archlinux.org/packages/golang-github-knq-ini/)
* [golang-github-mreiferson-go-httpclient](https://aur.archlinux.org/packages/golang-github-mreiferson-go-httpclient/)
* [golang-github-rogpeppe-go-internal](https://aur.archlinux.org/packages/golang-github-rogpeppe-go-internal/)
* [golang-github-swaywm-go-wlroots](https://aur.archlinux.org/packages/golang-github-swaywm-go-wlroots/)
* [golang-gopkg-errgo.v2](https://aur.archlinux.org/packages/golang-gopkg-errgo.v2/)
* [golang-rsc-2fa](https://aur.archlinux.org/packages/golang-rsc-2fa/)
* [google-font-download](https://aur.archlinux.org/packages/google-font-download/)
* [hlsdl-git](https://aur.archlinux.org/packages/hlsdl-git/)
* [mailpile](https://aur.archlinux.org/packages/mailpile/)
* [mailpile-git](https://aur.archlinux.org/packages/mailpile-git/)
* [miniterm](https://aur.archlinux.org/packages/miniterm/)
* [mongodb-compass-beta](https://aur.archlinux.org/packages/mongodb-compass-beta/)
* [mongodb-compass-beta-bin](https://aur.archlinux.org/packages/mongodb-compass-beta-bin/)
* [mongodb-compass-community](https://aur.archlinux.org/packages/mongodb-compass-community/)
* [mongodb-compass-community-beta](https://aur.archlinux.org/packages/mongodb-compass-community-beta/)
* [mongodb-compass-community-beta-bin](https://aur.archlinux.org/packages/mongodb-compass-community-beta-bin/)
* [mongodb-compass-community-bin](https://aur.archlinux.org/packages/mongodb-compass-community-bin/)
* [mongodb-compass-community-git](https://aur.archlinux.org/packages/mongodb-compass-community-git/)
* [mongodb-compass-git](https://aur.archlinux.org/packages/mongodb-compass-git/)
* [mongodb-compass-isolated](https://aur.archlinux.org/packages/mongodb-compass-isolated/)
* [mongodb-compass-isolated-beta](https://aur.archlinux.org/packages/mongodb-compass-isolated-beta/)
* [mongodb-compass-isolated-beta-bin](https://aur.archlinux.org/packages/mongodb-compass-isolated-beta-bin/)
* [mongodb-compass-isolated-bin](https://aur.archlinux.org/packages/mongodb-compass-isolated-bin/)
* [mongodb-compass-isolated-git](https://aur.archlinux.org/packages/mongodb-compass-isolated-git/)
* [mongodb-compass-readonly](https://aur.archlinux.org/packages/mongodb-compass-readonly/)
* [mongodb-compass-readonly-beta](https://aur.archlinux.org/packages/mongodb-compass-readonly-beta/)
* [mongodb-compass-readonly-beta-bin](https://aur.archlinux.org/packages/mongodb-compass-readonly-beta-bin/)
* [mongodb-compass-readonly-bin](https://aur.archlinux.org/packages/mongodb-compass-readonly-bin/)
* [mongodb-compass-readonly-git](https://aur.archlinux.org/packages/mongodb-compass-readonly-git/)
* [ocaml-dtoa](https://aur.archlinux.org/packages/ocaml-dtoa/)
* [ocaml-ounit](https://aur.archlinux.org/packages/ocaml-ounit/)
* [ocaml-ppx_gen_rec](https://aur.archlinux.org/packages/ocaml-ppx_gen_rec/)
* [ocaml-stdlib-shims](https://aur.archlinux.org/packages/ocaml-stdlib-shims/)
* [ocaml-wtf8](https://aur.archlinux.org/packages/ocaml-wtf8/)
* [orbterm](https://aur.archlinux.org/packages/orbterm/)
* [orbterm-git](https://aur.archlinux.org/packages/orbterm-git/)
* [papirus-libreoffice-theme](https://aur.archlinux.org/packages/papirus-libreoffice-theme/)
* [python2-imgsize](https://aur.archlinux.org/packages/python2-imgsize/)
* [python2-pysmart](https://aur.archlinux.org/packages/python2-pysmart/)
* [python2-rapidtables](https://aur.archlinux.org/packages/python2-rapidtables/)
* [python2-stem](https://aur.archlinux.org/packages/python2-stem/)
* [python-imgsize](https://aur.archlinux.org/packages/python-imgsize/)
* [python-neotermcolor](https://aur.archlinux.org/packages/python-neotermcolor/)
* [python-py_cui](https://aur.archlinux.org/packages/python-py_cui/)
* [python-pysmart](https://aur.archlinux.org/packages/python-pysmart/)
* [python-rapidtables](https://aur.archlinux.org/packages/python-rapidtables/)
* [railway-sans-font](https://aur.archlinux.org/packages/railway-sans-font/)
* [rust-rage](https://aur.archlinux.org/packages/rust-rage/)
* [rust-rage-bin](https://aur.archlinux.org/packages/rust-rage-bin/)
* [rust-rage-git](https://aur.archlinux.org/packages/rust-rage-git/)
* [shd](https://aur.archlinux.org/packages/shd/)
* [shd-git](https://aur.archlinux.org/packages/shd-git/)
* [spambayes](https://aur.archlinux.org/packages/spambayes/)
* [spambayes-svn](https://aur.archlinux.org/packages/spambayes-svn/)
* [tarnation](https://aur.archlinux.org/packages/tarnation/)
* [tarnation-git](https://aur.archlinux.org/packages/tarnation-git/)
* [wd5741](https://aur.archlinux.org/packages/wd5741/)
* [weatherspect](https://aur.archlinux.org/packages/weatherspect/)
* [weatherspect-git](https://aur.archlinux.org/packages/weatherspect-git/)
* [wxtoimg-beta](https://aur.archlinux.org/packages/wxtoimg-beta/)

## Local development

Builds packages changed since the last commit and updates package list in README:

```shell
./pkgbuild-repo.sh dev
```

## AUR deployment

Builds packages changed in the last commit and pushes packages that changed to the AUR:

```shell
./pkgbuild-repo.sh prod
```

## Arguments

`-a` - builds all packages regardless of if they changed or not

## Copyright

### Scripts used to manage the repo (`pkgbuild-repo.sh`, `util.sh`, `*/bundle.sh`)

Licensed under [GPLv3](LICENSE.GPLv3).

### weatherspect man pages (`weatherspect{,-git}/weatherspect.1`)

Based on the [weatherspect README](https://github.com/AnotherFoxGuy/weatherspect/blob/master/README.md) licensed under [GPLv2](https://github.com/AnotherFoxGuy/weatherspect/blob/master/gpl.txt) (changes: transformed the Markdown readme into the man page format, date: 2019/12/30), therefore licensed under [GPLv2](LICENSE.GPLv2) as well.

### All other files

Released into the public domain or where not possible, licensed under the [Unlicense](LICENSE.UNLICENSE).
